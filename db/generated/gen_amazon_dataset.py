from werkzeug.security import generate_password_hash
import csv
from faker import Faker
from datetime import datetime
import random
import json


USER_NUM = 1000
SELLER_NUM = 1000
# PRODUCT_NUM = 10000

Faker.seed(0)
fake = Faker()

def get_csv_writer(f):
    return csv.writer(f, dialect='unix')

def gen_users(num_users):
    email_set = set()
    with open('Users.csv', 'w') as f:
        writer = get_csv_writer(f)
        print('Users...', end=' ', flush=True)
        for uid in range(num_users):
            if uid % 100 == 0:
                print(f'{uid}', end=' ', flush=True)
            profile = fake.profile()
            email = profile['mail']
            if email in email_set:
                email = email.split("@")[0] + str(uid) + "@" + email.split("@")[1]
            email_set.add(email)
            plain_password = f'pass{uid}'
            password = generate_password_hash(plain_password)
            name = profile['name']
            address = profile['address']
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            writer.writerow([uid, name, address, email, password, '0', dt_string, dt_string])
        print(f'{num_users} generated')

def gen_stock_with_category():
    category_dict = {'Others':0}
    next_category_id = 1
    with open('Stock.csv', 'w') as cvs_f:
        writer = get_csv_writer(cvs_f)
        stock_id = 0
        with open('output.strict') as f:
            line_num = 0
            for line in f:
                dict = json.loads(line)
                try: 
                    now = datetime.now()
                    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
                    category_id = 0
                    name = dict['title']
                    if len(name) > 255:
                        name = name[:250] + "..."
                    seller_id = random.randint(0, USER_NUM)

                    category = ""
                    try:
                        category = dict['categories'][0][0].capitalize()
                    except Exception as e: 
                        category = "Others"
                    
                    if category in category_dict:       #existing category
                        category_id = category_dict.get(category)
                    else:
                        category_id = next_category_id
                        category_dict[category] = next_category_id
                        next_category_id += 1
                    
                    imUrl = dict.get('imUrl', 'https://i.ibb.co/XFr62Zs/Screen-Shot-2022-04-11-at-5-18-17-PM.png')
                    
                    description = dict.get('description', "No description...")
                    if len(description) > 255:
                        description = description[:250] + '...'

                    price = float(dict['price'])
                    writer.writerow([stock_id, name , seller_id, category_id, str(random.randint(1, 100)), imUrl, str(round(price, 2)), description, dt_string, dt_string, 'false'])
                    stock_id += 1
                    line_num += 1
                except Exception as e: 
                    pass
                # if line_num == PRODUCT_NUM:
                #     break
    with open('Category.csv', 'w') as cvs_f_c:
        writer = get_csv_writer(cvs_f_c)
        for c_name in category_dict:
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            writer.writerow([int(category_dict[c_name]), c_name, dt_string, dt_string])

def gen_sellers(num_sellers):
    email_set = set()
    with open('Sellers.csv', 'w') as f:
        writer = get_csv_writer(f)
        print('Sellers...', end=' ', flush=True)
        for sid in range(num_sellers):
            if sid % 10 == 0:
                print(f'{sid}', end=' ', flush=True)
            profile = fake.profile()
            b_email = profile['mail']
            if b_email in email_set:
                b_email = b_email.split("@")[0] + str(sid) + "@" + b_email.split("@")[1]
            email_set.add(b_email)
            b_name = profile['name']
            b_address = profile['address']
            writer.writerow([sid, b_name, b_email, b_address])
        print(f'{num_sellers} generated')
    return


# gen_users(USER_NUM)
# gen_stock_with_category()
gen_sellers(SELLER_NUM)

