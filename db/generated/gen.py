from werkzeug.security import generate_password_hash
import csv
from faker import Faker
from datetime import datetime
import random

num_users = 5
num_sellers = 5
num_products = 6
CATEGORY_NAME = ['Clothes', 'Food']
PRODUCT_CLOTHES = ['Hoodie', 'Sweater', 'T-Shirt']
PRODUCT_FOOD = ['Apple', 'Coffee', 'Burger']

Faker.seed(0)
fake = Faker()


def get_csv_writer(f):
    return csv.writer(f, dialect='unix')


def gen_users(num_users):
    with open('Users.csv', 'w') as f:
        writer = get_csv_writer(f)
        print('Users...', end=' ', flush=True)
        for uid in range(num_users):
            if uid % 10 == 0:
                print(f'{uid}', end=' ', flush=True)
            profile = fake.profile()
            email = profile['mail']
            plain_password = f'pass{uid}'
            password = generate_password_hash(plain_password)
            name = profile['name']
            address = profile['address']
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            writer.writerow([uid, name, address, email, password, '0', dt_string, dt_string])
        print(f'{num_users} generated')
    return

def gen_category():
    with open('Category.csv', 'w') as f:
        writer = get_csv_writer(f)
        for id, name in enumerate(CATEGORY_NAME):
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            writer.writerow([id, name, dt_string, dt_string])

# def gen_product():
#     with open('Product.csv', 'w') as f:
#         writer = get_csv_writer(f)
#         id = 0
#         for name in PRODUCT_CLOTHES:
#             now = datetime.now()
#             dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
#             writer.writerow([id, 0, name, dt_string, dt_string])
#             id = id + 1
#         for name in PRODUCT_FOOD:
#             now = datetime.now()
#             dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
#             writer.writerow([id, 1, name, dt_string, dt_string])
#             id = id + 1

def gen_stock():
    with open('Stock.csv', 'w') as f:
        writer = get_csv_writer(f)
        id = 0

        #cloth
        for product in ['Hoodie', 'Sweater', 'T-Shirt']: 
            for sid in range(num_sellers):
                now = datetime.now()
                dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
                writer.writerow([id, product + str(sid) , sid, 0, str(random.randint(1, 20)), fake.image_url(), str(round(random.uniform(1,100), 2)), product + str(sid) + " description...", dt_string, dt_string, 'false'])
                id += 1

        for product in ['Apple', 'Coffee', 'Burger']:
            for sid in range(num_sellers):
                now = datetime.now()
                dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
                writer.writerow([id, product + str(sid) , sid, 1, str(random.randint(1, 20)), fake.image_url(), str(round(random.uniform(1,100), 2)), product + str(sid) + " description...", dt_string, dt_string, 'false'])
                id += 1

def gen_sellers(num_sellers):
    with open('Sellers.csv', 'w') as f:
        writer = get_csv_writer(f)
        print('Sellers...', end=' ', flush=True)
        for sid in range(num_sellers):
            if sid % 10 == 0:
                print(f'{sid}', end=' ', flush=True)
            profile = fake.profile()
            b_email = profile['mail']
            b_name = profile['name']
            b_address = profile['address']
            writer.writerow([sid, b_name, b_email, b_address])
        print(f'{num_sellers} generated')
    return

# gen_users(num_users)
gen_category()
# gen_product()
gen_stock()
# gen_sellers(num_sellers)
