\COPY Users FROM 'Users.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval(pg_get_serial_sequence('public.users', 'user_id'),
                         (SELECT MAX(user_id)+1 FROM Users),
                         false);

\COPY Category FROM 'Category.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval(pg_get_serial_sequence('public.category', 'category_id'),
                         (SELECT MAX(category_id)+1 FROM Category),
                         false);

\COPY Stock FROM 'Stock.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval(pg_get_serial_sequence('public.stock', 'stock_id'),
                         (SELECT MAX(stock_id)+1 FROM Stock),
                         false);

\COPY Sellers FROM 'Sellers.csv' WITH DELIMITER ',' NULL '' CSV
SELECT pg_catalog.setval(pg_get_serial_sequence('public.sellers', 'seller_id'),
                         (SELECT MAX(seller_id)+1 FROM Sellers),
                         false);
