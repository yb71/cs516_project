import json
from datetime import datetime
from math import ceil

from flask import render_template, redirect, url_for, flash, request
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, FloatField, SelectField, DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, NumberRange, Optional

from .models.item import Item
from .models.order import Order
from .models.page import Page
from .models.user import User
from .models.buyer_order_detail import BuyerOrderDetail


from flask import Blueprint
bp = Blueprint('users', __name__)
PAGE_NUM = 10

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index.index'))
    form = LoginForm()
    print(form.email)
    if form.validate_on_submit():
        user = User.get_by_auth(form.email.data, form.password.data)
        if user is None:
            flash('Invalid email or password')
            return redirect(url_for('users.login'))
        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index.index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


class RegistrationForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(),
                                       EqualTo('password')])
    address = StringField('Address', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, email):
        if User.email_exists(email.data):
            raise ValidationError('Already a user with this email.')


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
        if User.register(form.email.data,
                         form.password.data,
                         form.name.data,
                         form.address.data,
                         dt_string):
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('users.login'))


@bp.route('/account', methods=['GET', 'POST'])
def show_user():
    if current_user.is_authenticated:
        id = current_user.get_id()
        user = User.get(id)
        return render_template('account.html', data=user)
    else:
        return redirect(url_for('users.login'))


class EditAccountForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    address = StringField('Address', validators=[DataRequired()])
    submit = SubmitField('Save')

    def validate_email(self, email):
        if User.email_exists(email.data) and get_email() != email.data:
            raise ValidationError('Already a user with this email.')


@bp.route('/edit_account', methods=['GET', 'POST'])
def edit_account():
    if current_user.is_authenticated:
        id = current_user.get_id()
        user = User.get(id)
        form = EditAccountForm()
        if form.validate_on_submit():
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if User.update_info(id, form.email.data, form.name.data, form.address.data, dt_string):
                return redirect(url_for('users.show_user'))
        return render_template('edit_account.html', data=user, form=form)
    else:
        return redirect(url_for('users.login'))


class ChangePwForm(FlaskForm):
    pw = PasswordField('New password', validators=[DataRequired()])
    pw2 = PasswordField('Confirm new password', validators=[DataRequired(), EqualTo('pw', message="The two passwords don't match.")])
    submit = SubmitField('Save')


@bp.route('/change_pw', methods=['GET', 'POST'])
def change_pw():
    if current_user.is_authenticated:
        id = current_user.get_id()
        form = ChangePwForm()
        if form.validate_on_submit():
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if User.change_pw(id, form.pw.data, dt_string):
                flash('Password successfully changed.')
                return redirect(url_for('users.show_user'))
        return render_template('change_pw.html', form=form)
    else:
        return redirect(url_for('users.login'))


class AddFundForm(FlaskForm):
    fund = FloatField('Enter the amount you wish to add: ',
                      validators=[DataRequired(message="Please enter a positive number."), NumberRange(min=0.0)])
    submit = SubmitField('Add')


@bp.route('/add_fund', methods=['GET', 'POST'])
def add_fund():
    if current_user.is_authenticated:
        id = current_user.get_id()
        user = User.get(id)
        balance = user.balance
        form = AddFundForm()
        if form.validate_on_submit():
            fund = form.fund.data
            new_balance = round(balance + fund, 2)
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if User.update_balance(id, new_balance, dt_string):
                return redirect(url_for('users.show_user'))
        return render_template('add_fund.html', form=form)
    else:
        return redirect(url_for('users.login'))


class WithdrawFundForm(FlaskForm):
    fund = FloatField('Enter the amount you wish to withdraw: ',
                      validators=[DataRequired(message="Please enter a positive number."), NumberRange(min=0.0)])
    submit = SubmitField('Withdraw')

    def validate_fund(self, fund):
        if fund.data > get_balance():
            raise ValidationError('Sorry, you cannot withdraw more than your current balance.')


@bp.route('/withdraw_fund', methods=['GET', 'POST'])
def withdraw_fund():
    if current_user.is_authenticated:
        id = current_user.get_id()
        balance = get_balance()
        form = WithdrawFundForm()
        if form.validate_on_submit():
            fund = form.fund.data
            new_balance = round(balance - fund, 2)
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if User.update_balance(id, new_balance, dt_string):
                return redirect(url_for('users.show_user'))
        return render_template('withdraw_fund.html', form=form)
    else:
        return redirect(url_for('users.login'))


def get_balance():
    id = current_user.get_id()
    user = User.get(id)
    return user.balance


def get_email():
    id = current_user.get_id()
    user = User.get(id)
    return user.email


class SearchForm(FlaskForm):
    input = StringField('Search', validators=[Optional()], render_kw={"placeholder": "Search by item name or seller..."})
    from_date = DateField('from order date', validators=[Optional()], render_kw={"placeholder": "YYYY-mm-dd"})
    to_date = DateField('to order date', validators=[Optional()], render_kw={"placeholder": "YYYY-mm-dd"})
    submit = SubmitField('Search')

    def validate_dates(self, from_date, to_date):
        if from_date.data > to_date.data:
            raise ValidationError('The second date must not be earlier the first date.')


class OrderSummary:
    def __init__(self, order_num, total_quant, total_price, order_time, status):
        self.order_num = order_num
        self.total_quant = total_quant
        self.total_price = total_price
        self.order_time = order_time
        self.status = status


@bp.route('/purchase_history', methods=['GET', 'POST'])
def purchase_history():
    if current_user.is_authenticated:
        form = SearchForm()
        id = current_user.get_id()
        request_dict = request.args.to_dict(flat=False)
        if request_dict.get('page') is None:
            current_page = 1
        else:
            current_page = int(request_dict.get('page')[0])
        last_page = 0
        page_list = []
        order_nums = []

        if form.validate_on_submit():
            input_text = form.input.data.lower()
            order_count = Order.get_order_num_count_by_buyer_search(id, input_text, form.from_date.data, form.to_date.data)
            if order_count > 0:
                last_page = ceil(order_count / PAGE_NUM)
                page_list = Page.get_page_info(current_page, last_page)
                order_nums = Order.get_order_num_by_buyer_search(id, input_text, form.from_date.data, form.to_date.data,
                                                                 PAGE_NUM, PAGE_NUM * (current_page - 1))
            else:
                flash(
                    "Sorry, your search did not return any results. If you are filtering by date, please make sure that the end date is not earlier than the start date.")
        else:
            order_count = Order.get_order_num_count_by_buyer_search(id, "", None, None)
            if order_count > 0:
                last_page = ceil(order_count / PAGE_NUM)
                page_list = Page.get_page_info(current_page, last_page)
                order_nums = Order.get_order_num_by_buyer_search(id, "", None, None, PAGE_NUM, PAGE_NUM*(current_page-1))
            else:
                flash("You have not placed any orders yet. Let's start treasure hunting!")

        order_summaries = []
        for order_num in order_nums:
            items = Item.select_by_order_num(order_num[0])
            total_quant = 0
            total_price = 0
            order_time = ""
            status = "Fulfilled"
            for item in items:
                total_quant = total_quant + item.quantity
                total_price = round(total_price + item.unit_price * item.quantity, 2)
                order_time = item.order_time
                if item.status != 3:
                    status = "Unfulfilled"
            order_summaries.append(OrderSummary(order_num[0], total_quant, total_price, order_time, status))
        order_summaries.sort(key=lambda x: x.order_time, reverse=True)

        return render_template('purchase_history.html', form=form, order_summaries=order_summaries, page_list=page_list, current_page = current_page, last_page = last_page)
    else:
        return redirect(url_for('users.login'))


@bp.route('/user_order_details', methods=['GET', 'POST']) 
def order_details():
    order_num = request.args['order_num'] 
    if current_user.is_authenticated:

        user_id = current_user.get_id()        
        order_details = BuyerOrderDetail.get_item_by_user_id_order_num(user_id, order_num)
        
        if not order_details:
            order_details = []
        order_status = "Fulfilled"
        for order_detail in order_details:
            if order_detail.status != 3:
                order_status = "Unfulfilled"
                break
        

        user_ids = tuple(set([user_id]))
        user_id_name_dict = User.select_names_by_ids(user_ids)                                   #all category for present
        return render_template('buyer_order_details.html',
                           order_status = order_status,
                           order_details=order_details,
                           order_num = order_num,
                           user_id_name_dict=user_id_name_dict)