from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import login_user, logout_user, current_user
from flask_login import current_user
from .models.category import Category
from .models.user import User
from .models.item import Item
from .models.item_info import ItemInfo
from .models.stock import Stock
from flask import request
from flask import Blueprint
import datetime
from math import ceil
import json
from .constants import IN_CART, CART_ITEM_OUT_OF_STOCK, CART_ITEM_INVALID, CART_ITEM_VALID

bp = Blueprint('cart', __name__)

@bp.route('/cart', methods = ['GET'])  #done
def cart():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        user_info = User.get(buyer_id)
        ready_to_checkout, items_in_cart, total_price, total_item_num = ItemInfo.get_items_in_cart_by_uid(buyer_id)
        if(total_price > user_info.balance):
            ready_to_checkout = False
        seller_id_name_dict = {}
        if len(items_in_cart) > 0:
            seller_ids = tuple(set([item.seller_id for item in items_in_cart]))
            seller_id_name_dict = User.select_names_by_ids(seller_ids)
        return render_template('cart.html', 
                                ready_to_checkout = ready_to_checkout, 
                                total_price = total_price, 
                                balance = user_info.balance,
                                items_in_cart = items_in_cart,
                                seller_id_name_dict = seller_id_name_dict)
    else:
        return redirect(url_for('users.login'))

@bp.route('/cart/add', methods=['POST']) #done
def add_cart():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        request_dict = json.loads(request.data.decode('utf-8'))

        stock_id = request_dict['stock_id']
        seller_id = request_dict['seller_id']
        quantity = request_dict['quantity']
        status = IN_CART
    
        item_existed = Item.check_exits_by_bid_stockid_status(buyer_id, stock_id, status)
        if not item_existed:
            Item.insert_item(buyer_id, stock_id, seller_id, int(quantity), status)
        else:
            existed_item = Item.select_by_bid_stockid_status(buyer_id, stock_id, status)
            Item.update_quantity_by_bid_stockid_status(buyer_id, stock_id, status, int(existed_item.quantity) + int(quantity))
        return redirect(url_for('index.detail', stock_id = stock_id))
    else:
        return redirect(url_for('users.login'))


""" update item in cart, only update quantity"""
@bp.route('/cart/edit', methods = ['POST'])
def update_cart():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        request_dict = json.loads(request.data.decode('utf-8'))
        item_list = request_dict['items']
        for item in item_list:
            item_id = int(item['item_id'])
            quantity = int(item['quantity']) 
            Item.update_quantity_by_bid_itemid(buyer_id, item_id, quantity)
        resp = jsonify({"success": True})
        return resp
    else:
        return redirect(url_for('users.login'))

""" delete item in cart"""
@bp.route('/cart/delete', methods = ['POST'])
def delete_cart():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        request_dict = json.loads(request.data.decode('utf-8'))
        item_id = int(request_dict['item_id'])
        status = IN_CART
        Item.delete_item_by_item_id(item_id)
        resp = jsonify({"success": True })
        return resp
    else:
        return redirect(url_for('users.login'))

""" delete item in cart"""
@bp.route('/cart/clear', methods = ['POST'])
def clear_cart():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        status = IN_CART
        res = Item.clear_cart_by_bid(buyer_id, status)
        if res:
            return jsonify({"success": True })
        return jsonify({"success": False })
    else:
        return redirect(url_for('users.login'))

