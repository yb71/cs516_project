from flask import render_template, jsonify
from flask_login import current_user
from .models.category import Category
from .models.item import Item
from .models.stock import Stock
from .models.user import User
from .models.checkout_processor import checkoutProcessor
from .models.item_info import ItemInfo
from flask import request
from flask import Blueprint
import datetime
from math import ceil
import time
from .constants import IN_CART, ORDERED, DELIVERED_FULFILLED
import json

bp = Blueprint('checkout', __name__)

@bp.route('/checkout', methods = ['GET'])
def checkout_get():
    if current_user.is_authenticated:

        buyer_id = current_user.id
        order_num = request.args["order_num"]

        buyer_info = User.get(buyer_id)
        uncheck_item_ids = request.args["uncheck_item_ids"]
        total_price = 0 
        #checked items
        checked_items = ItemInfo.get_items_by_bid_order_num(buyer_id, order_num)
        if len(checked_items)>0:
            total_price = round(sum(i.unit_price * i.cart_qty for i in checked_items),2)

        #unchecked items
        unchecked_items = []
        if uncheck_item_ids:
            uncheck_item_ids_list = tuple(set( uncheck_item_ids.split(',')))
            unchecked_items = ItemInfo.get_items_by_bid_itemids(buyer_id, uncheck_item_ids_list)
        return render_template("checkout.html", 
                            order_num = order_num, 
                            items = checked_items, 
                            unchecked_items = unchecked_items,
                            unchecked_num = len(unchecked_items),
                            checked_num = len(checked_items),
                            total_price = total_price,
                            balance = round(buyer_info.balance,2))
    else:
        return redirect(url_for('users.login'))

@bp.route('/checkout', methods = ['POST'])
def checkout_post():
    if current_user.is_authenticated:
        buyer_id = current_user.id
        request_dict = json.loads(request.data.decode('utf-8'))
        item_list = request_dict['items']
        total_price = request_dict['total_price']
        new_order_num, uncheck_item_ids, total_price = checkoutProcessor.checkout(buyer_id, request_dict, item_list, total_price)
        resp = jsonify({"success": True, "order_num": new_order_num, "uncheck_item_ids": uncheck_item_ids})
        return resp
    else:
        return redirect(url_for('users.login'))