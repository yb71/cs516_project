from datetime import datetime
from math import ceil
from flask import render_template, redirect, url_for, flash, request
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, FloatField, TextAreaField, SelectField, DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, URL, InputRequired,NumberRange, Optional
from .models.stock import Stock
from .models.category import Category
from .models.user import User
from .models.seller import Seller
from .models.item import Item
from .models.order import Order
from .models.seller_order_detail import SellerOrderDetail
from .models.hot_item import HotItem
from .models.page import Page
from .constants import PAGE_NUM

from flask import Blueprint
bp = Blueprint('sellers', __name__)

@bp.route('/seller', methods=['GET'])
def seller():
    if current_user.is_authenticated:
        seller_id = current_user.get_id()
        user = User.get(seller_id)
        seller = Seller.get(seller_id)
        if not seller:
            flash('Your seller profile is empty, please click \'Edit info\' below to create your seller profile!')
            seller = Seller.get_empty(seller_id)
        avail_stocks = Stock.select_by_seller(seller_id)
        if not avail_stocks:
            num_stocks = 0
        else:
            num_stocks = len(avail_stocks)
        orders = Order.select_by_seller_id(seller_id)
        if not orders:
            orders = []
        num_fulfilled = len([order for order in orders if order.status == 'Fulfilled'])
        num_unfulfilled = len(orders) - num_fulfilled
        return render_template('seller_account.html', user=user, seller=seller, num=num_stocks, num_fulfilled=num_fulfilled, num_unfulfilled=num_unfulfilled)
    else:
        flash('Please log in to access your seller account')
        return redirect(url_for('users.login'))

class EditSellerAccountForm(FlaskForm):
    b_name = StringField('Business Name', validators=[DataRequired()])
    b_email = StringField('Business Email', validators=[DataRequired(), Email()])
    b_address = StringField('Business Address', validators=[DataRequired()])
    submit = SubmitField('Save')


@bp.route('/edit_seller_account', methods=['GET', 'POST'])
def edit_seller_account():
    if current_user.is_authenticated:
        id = current_user.get_id()
        seller = Seller.get(id)
        empty = False
        if not seller:
            empty = True
            seller = Seller.get_empty(id)
        form = EditSellerAccountForm()
        if form.validate_on_submit():
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if empty:
                if Seller.add_seller(id, form.b_name.data, form.b_email.data, form.b_address.data):
                    flash('Success!')
                    return redirect(url_for('sellers.seller'))
            else:
                if Seller.update_seller(id, form.b_name.data, form.b_email.data, form.b_address.data):
                    flash('Success!')
                    return redirect(url_for('sellers.seller'))
        return render_template('edit_seller_account.html', data=seller, form=form)
    else:
        return redirect(url_for('users.login'))

@bp.route('/inventory', methods=['GET'])
def inventory():
    if current_user.is_authenticated:

        seller_id = current_user.get_id()
        request_dict = request.args.to_dict(flat=False)
        if request_dict.get('page') is None:
            current_page = 1
        else:
            current_page = int(request_dict.get('page')[0])
        last_page = 0
        page_list = []
        avail_stocks_num = Stock.select_count_by_seller(seller_id)
        avail_stocks = Stock.select_by_seller_limited(seller_id, PAGE_NUM, PAGE_NUM * (current_page - 1))
        if not avail_stocks:
            avail_stocks = []
            flash('Your inventory is empty, please click \'Add\' below to start selling!')
        seller_ids = tuple(set([seller_id]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)
        categories = Category.get_all()
        last_page = ceil(avail_stocks_num/PAGE_NUM)
        page_list = Page.get_page_info(current_page, last_page)                                     #all category for present
        return render_template('inventory.html',
                           avail_stocks=avail_stocks,
                           avail_categories=categories,
                           seller_id_name_dict=seller_id_name_dict,
                           page_list = page_list,
                           current_page = current_page,
                           last_page = last_page) 

    else:
        flash('Please log in to access your inventory')
        return redirect(url_for('users.login'))

class EditInventoryForm(FlaskForm):
    '''
    SELECT stock_id, name, seller_id, cid, quantity, image, price, description, create_time, update_time
    FROM Stock s
    '''
    name = StringField('Product Name', validators=[DataRequired()])
    quantity = IntegerField('Quantity', validators=[DataRequired(),NumberRange(min = 0)])
    #image = StringField('Image', validators=[DataRequired()])
    price = FloatField('Price', validators=[DataRequired(),NumberRange(min = 0.0)])
    description = TextAreaField('Description', validators=[DataRequired()])
    image = StringField('Image URL', validators=[DataRequired(),URL(message='Must be a valid URL')])
    submit = SubmitField('Save')

@bp.route('/edit_inventory', methods=['GET','POST'])    
def edit_inventory():
    sid = request.args['stock_id']

    stock = Stock.get_stock_by_sid(sid)

    form = EditInventoryForm()
    #form.description.data = stock.description
    if form.validate_on_submit():
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
        if stock.update_inventory(sid, form.name.data, form.quantity.data, round(form.price.data,2), form.image.data, form.description.data, dt_string):
            flash('Success!')
            return redirect(url_for('sellers.inventory'))
    return render_template('edit_inventory.html', data=stock, form=form)

@bp.route('/delete_inventory', methods=['GET','POST'])
def delete_inventory():
    sid = request.args['stock_id']

    if Stock.delete_inventory(sid):
        flash("Success!")
        return redirect(url_for('sellers.inventory'))

@bp.route('/inventory_detail', methods=['GET']) #done
def inventory_detail(): 
    stock_id = request.args['stock_id']
    stock_detail = Stock.select_by_primary_key(stock_id)
    sellor_name = User.get(stock_detail.seller_id).name
    category = Category.get(stock_detail.cid).name
    return render_template('inventory_detail.html', 
                            stock_detail = stock_detail, 
                            sellor_name = sellor_name,
                            category = category)


class AddInventoryForm(FlaskForm):

    category = SelectField('Category', coerce=int, choices=[InputRequired])
    name = StringField('Product Name', validators=[DataRequired()])
    quantity = IntegerField('Quantity', validators=[DataRequired(),NumberRange(min = 0)])
    #image = StringField('Image', validators=[DataRequired()])
    price = FloatField('Price', validators=[DataRequired(),NumberRange(min = 0.0)])
    description = TextAreaField('description', validators=[DataRequired()])
    image = StringField('Image URL', validators=[DataRequired(),URL(message='Must be a valid URL')])
    submit = SubmitField('Save')

@bp.route('/add_inventory', methods=['GET','POST'])    
def add_inventory():
    if current_user.is_authenticated:
        #seller_id = 0
        seller_id = current_user.get_id()
        categories = Category.get_all()
        c_list_ids = [(c.category_id,c.name) for c in categories]
        form = AddInventoryForm()
        form.category.choices = c_list_ids
        if form.validate_on_submit():
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            if Stock.add_inventory(seller_id, form.name.data, form.category.data, form.quantity.data, form.image.data, round(form.price.data,2), form.description.data, dt_string):
                flash('Success!')
                return redirect(url_for('sellers.inventory'))
        return render_template('add_inventory.html', form=form)
    else:
        flash('Please log in to access your inventory')
        return redirect(url_for('users.login'))

class SearchForm(FlaskForm):
    input = StringField('Search', validators=[Optional()], render_kw={"placeholder": "Search by item name or buyer..."})
    from_date = DateField('from order date', validators=[Optional()], render_kw={"placeholder": "YYYY-mm-dd"})
    to_date = DateField('to order date', validators=[Optional()], render_kw={"placeholder": "YYYY-mm-dd"})
    submit = SubmitField('Search')

    def validate_dates(self, from_date, to_date):
        if from_date.data > to_date.data:
            raise ValidationError('The second date must not be earlier the first date.')

@bp.route('/orders', methods=['GET', 'POST']) 
def orders(): 
    if current_user.is_authenticated:

        seller_id = current_user.get_id()        
        #orders = Order.select_by_seller_id(seller_id)
        form = SearchForm()
        request_dict = request.args.to_dict(flat=False)
        if request_dict.get('page') is None:
            current_page = 1
        else:
            current_page = int(request_dict.get('page')[0])
        last_page = 0
        page_list = []

        

        if form.validate_on_submit():
            input_text = form.input.data.lower()
            order_count = Order.get_count_by_seller_search(seller_id, input_text, form.from_date.data, form.to_date.data)
            orders = Order.get_order_num_by_seller_search(seller_id, input_text, form.from_date.data, form.to_date.data, PAGE_NUM, PAGE_NUM * (current_page - 1))
        else:
            order_count = Order.select_count_by_seller_id(seller_id)
            orders = Order.select_by_seller_id_limited(seller_id, PAGE_NUM, PAGE_NUM * (current_page - 1))
        if not orders:
            orders = []
            flash("Sorry, your search did not return any results. If you are filtering by date, please make sure that the end date is not earlier than the start date.")
        else:
            orders = sorted(orders, key = lambda x: x.update_time, reverse = True)
            last_page = ceil(order_count / PAGE_NUM)
            page_list = Page.get_page_info(current_page, last_page)
        
        seller_ids = tuple(set([seller_id]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)
        categories = Category.get_all()                                     #all category for present
        return render_template('orders.html',
                           form = form,
                           categories=categories,
                           orders=orders,
                           seller_id_name_dict=seller_id_name_dict,
                           page_list = page_list,
                           current_page = current_page,
                           last_page = last_page)


@bp.route('/order_details', methods=['GET', 'POST']) 
def order_details():
    order_num = request.args['order_num'] 
    if current_user.is_authenticated:

        seller_id = current_user.get_id()        
        order_details = SellerOrderDetail.get_item_by_seller_id_order_num(seller_id, order_num)
        
        if not order_details:
            order_details = []
        order_status = "Fulfilled"
        for order_detail in order_details:
            if order_detail.status != 3:
                order_status = "Unfulfilled"
                break
        

        seller_ids = tuple(set([seller_id]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)                                   #all category for present
        return render_template('seller_order_details.html',
                           order_status = order_status,
                           order_details=order_details,
                           order_num = order_num,
                           seller_id_name_dict=seller_id_name_dict)


@bp.route('/fulfill', methods=['GET', 'POST'])
def fulfill():
    item_id = request.args['item_id'] 
    order_num = request.args['order_num']
    if current_user.is_authenticated:
        seller_id = current_user.get_id() 
        balance = User.get(seller_id).balance
        item = Item.select_by_item_id(item_id)
        price = item.unit_price*item.quantity
        if Item.fulfill_by_itemid(item_id):
            t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            if User.update_balance(seller_id,balance+price,t):
                flash('Successfully Fulfilled')
                return redirect(url_for('sellers.order_details')+'?order_num='+str(order_num))


@bp.route('/analysis_low', methods=['GET']) #done
def running_low(): 
    if current_user.is_authenticated:

        seller_id = current_user.get_id()        
        avail_stocks = Stock.select_by_seller(seller_id)
        
        if not avail_stocks:
            avail_stocks = []
        low_stocks = [s for s in avail_stocks if s.quantity <10]

        seller_ids = tuple(set([seller_id]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)
        categories = Category.get_all()                                     #all category for present
        return render_template('analysis_low.html',
                           avail_stocks=low_stocks,
                           avail_categories=categories,
                           seller_id_name_dict=seller_id_name_dict) 

@bp.route('/analysis_hot', methods=['GET']) #done
def hot_items(): 
    if current_user.is_authenticated:

        seller_id = current_user.get_id()        
        hot_items = HotItem.select_week_hot_items(seller_id)
        
        if not hot_items:
            hot_items = []

        seller_ids = tuple(set([seller_id]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)
        return render_template('analysis_hot.html',
                           hot_items=hot_items,
                           seller_id_name_dict=seller_id_name_dict) 