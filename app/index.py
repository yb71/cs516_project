from flask import render_template, flash
from flask_login import current_user
from .models.category import Category
from .models.stock import Stock
from .models.user import User
from .models.page import Page
from .models.sort import Sort
from flask import request
from flask import Blueprint
import datetime
from math import ceil

bp = Blueprint('index', __name__)

PAGE_NUM = 10
@bp.route('/', methods=['GET']) #done
def index(): 
    avail_stocks = Stock.get_stock_by_condition_with_pagination("", None, None, PAGE_NUM, 0)
    avail_stocks_num = Stock.get_total_count_condition("", None)
    current_page = 1
    last_page = ceil(avail_stocks_num/PAGE_NUM)
    page_list = Page.get_page_info(current_page, last_page)
    seller_ids = tuple(set([stock.seller_id for stock in avail_stocks]))
    seller_id_name_dict = User.select_names_by_ids(seller_ids)
    categories = Category.get_all() #all category for present
    sort_list = Sort.sortOptions(-1)
    return render_template('search.html',
                           avail_stocks=avail_stocks,
                           avail_categories=categories,
                           seller_id_name_dict=seller_id_name_dict,
                           page_list = page_list,
                           sort_list = sort_list,
                           current_page = 1,
                           last_page = last_page) 


@bp.route('/search', methods=['GET']) #done
def search(): 
    key_word = request.args['search_key']
    category = int(request.args['category'])
    current_page = int(request.args['page'])
    sort = int(request.args['sort'])

    categories = Category.get_all() #all category for present                       
    product_key_word = key_word.lower()
    search_category = None if category == -1 else category
    avail_stocks = Stock.get_stock_by_condition_with_pagination(product_key_word, search_category, sort, PAGE_NUM, PAGE_NUM*(current_page-1))
    avail_stocks_num = Stock.get_total_count_condition(product_key_word, search_category)
    page_list = []
    seller_id_name_dict = {}
    last_page = 0
    if avail_stocks_num > 0:
        last_page = ceil(avail_stocks_num/PAGE_NUM)
        page_list = Page.get_page_info(current_page, last_page)
        seller_ids = tuple(set([stock.seller_id for stock in avail_stocks]))
        seller_id_name_dict = User.select_names_by_ids(seller_ids)
    sort_list = Sort.sortOptions(sort)
    for r in sort_list:
        print(str(r.option) + " " +  str(r.value) + " " + str(r.selected) )
    return render_template( 'search.html',
                            avail_stocks=avail_stocks,
                            avail_categories=categories,
                            selected_category = category,
                            key_word = key_word,
                            seller_id_name_dict=seller_id_name_dict,
                            page_list = page_list,
                            sort_list = sort_list,
                            current_page = current_page,
                            last_page = last_page)  


@bp.route('/detail', methods=['GET']) #done
def detail(): 
    stock_id = request.args['stock_id']
    stock_detail = Stock.select_by_primary_key(stock_id)
    sellor_name = User.get(stock_detail.seller_id).name
    category = Category.get(stock_detail.cid).name
    return render_template('product_detail.html', 
                            stock_detail = stock_detail, 
                            sellor_name = sellor_name,
                            category = category)

