function addCart() {
    var stock_id = document.getElementById("stock_id").value;
    var seller_id = document.getElementById("seller_id").value;
    var quantity = document.getElementById("quantity").value;
    var data = {
        "stock_id": stock_id,
        "seller_id": seller_id,
        "quantity": quantity
    };
    $.ajax({
        url: "/cart/add",
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        data: JSON.stringify(data),
        success: function(response) {
            alert("Successfully added into cart!");
        },
        error: function(error) {
            console.log(error);
        }
    });
}