//for first search request, default page = 1
function submitSearch() {
    var req_data={
            "page": 1
        };
    sendSearchReq(req_data);  
}

function nextPage(p) {
    var page;
    if(!isNaN(p)) {       //not a number
        page = parseInt(p);
    } else{
        current_page = parseInt(document.getElementById("current_page").value);
        last_page = parseInt(document.getElementById("last_page").value);
        if(p == "Next") {
            page = current_page + 1;
        } else if (p == "Previous") {
            page = current_page - 1;
        } else if (p == "First") {
            page = 1;
        } else{
            page = last_page;
        }
    }
    var req_data={
        "page": page
    };
    sendSearchReq(req_data);
}

function sendSearchReq(req_data) {
    $.ajax({
        url: '/purchase_history?page=' + req_data['page'],
        contentType: 'application/json;charset=UTF-8',
        type: 'GET',
        success: function(response) { 
            $('body').html(response);
        },
        error: function(error) {
            console.log(error);
        }
    });
}
