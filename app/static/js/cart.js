function checkReadyToCheckout() {
    var table= document.getElementById("cartTable");
    var tblTR = table.getElementsByTagName("tr");
    for (var i = 1; i<tblTR.length; i ++) {
        var tr = tblTR[i];
        var tds = tr.getElementsByTagName("td");
        var item_status = tds[0].getElementsByTagName("input")[1].value;
        var stock_qty = tds[1].getElementsByTagName("input")[1].value;
        var cart_qty = tds[1].getElementsByTagName("input")[0].value;
        //un-deleted invalid products
        if(item_status == 3) {                          
            console.log("Not ready to checkout has to delete something");
            document.getElementById("checkout_btn").disabled = true;
            return;
        }
        //out of stock products
        if( parseInt(stock_qty) < parseInt(cart_qty)) {
            console.log("Not ready to checkout, out of stock");
            document.getElementById("checkout_btn").disabled = true;
            return;
        }
        else{
            tds[1].getElementsByTagName("label")[0].innerHTML = stock_qty + " remain in stock...";
            tds[1].getElementsByTagName("label")[0].style.color = "#000000";
        }
    }
    //check balance -- total
    var total_price = parseInt(document.getElementById("subTotal").innerHTML);
    var balance = parseInt(document.getElementById("balance").innerHTML);

    if(total_price > balance) {
        console.log("Lack of Balance");
        document.getElementById("checkout_btn").disabled = true;
        return;
    }
    document.getElementById("checkout_btn").disabled = false;
}

function updateSubTotal() {
    // console.log("IN updateSubTotal");
    var table= document.getElementById("cartTable");
    var tblTR = table.getElementsByTagName("tr");
    var subTotal = 0;
    for (var i = 1; i<tblTR.length; i ++) {
        var tr = tblTR[i];
        var tds = tr.getElementsByTagName("td");
        subTotal = subTotal + tds[1].getElementsByTagName("input")[0].value * tds[2].getElementsByTagName("input")[0].value;
    }
    document.getElementById("subTotal").innerHTML = subTotal.toFixed(2);
  }

function update_carts() {
    console.log("update");
    var table= document.getElementById("cartTable");
    var tblTR = table.getElementsByTagName("tr");
    var item_list = []
    for (var i = 1; i<tblTR.length; i ++) {
        var tr = tblTR[i];
        var tds = tr.getElementsByTagName("td");
        var item = {
            item_id: tds[0].getElementsByTagName("input")[0].value,
            quantity: tds[1].getElementsByTagName("input")[0].value
        }
        item_list.push(item);
    }

    $.ajax({
        url: '/cart/edit',
        data: JSON.stringify({items:item_list}),
        // data: JSON.stringify(total_price),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(response) {
            console.log(response);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function submit_carts() {
    var table= document.getElementById("cartTable");
    var tblTR = table.getElementsByTagName("tr");
    var item_list = []
    var total_price = document.getElementById("total_price").value;
    console.log(total_price);
    for (var i = 1; i<tblTR.length; i ++) {
        var tr = tblTR[i];
        var tds = tr.getElementsByTagName("td");
        // hiddenInputs = tds[0].getElementsByTagName("input");
        var item = {
            item_id: tds[0].getElementsByTagName("input")[0].value,
            quantity: tds[1].getElementsByTagName("input")[0].value,
            unit_price: tds[2].getElementsByTagName("input")[0].value
        }
        item_list.push(item);
    }

    $.ajax({
        url: '/checkout',
        data: JSON.stringify({items:item_list, total_price: total_price}),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(response) {
            order_num = response["order_num"];
            uncheck_item_ids = response["uncheck_item_ids"];
            var url = window.location.href.split("cart")[0] + "checkout?order_num=" + order_num + "&uncheck_item_ids=" + uncheck_item_ids;
            window.location.href = url;
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function delete_single_item(item_id) {
    $.ajax({
        url: '/cart/delete',
        data: JSON.stringify({item_id:item_id}),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(response) {
            console.log(response);
            document.getElementById("row_"+ item_id).remove();
            updateSubTotal();
            checkReadyToCheckout();
        },
        error: function(error) {
            console.log(error);
        }
    });

}