//for first search request, default page = 1
function submitSearch() {
    var category = document.getElementById("category").value;
    var search_key = document.getElementById("keyword").value;
    var sort = document.getElementById("sort").value;
    var req_data={
            "category": category,
            "search_key": search_key,
            "sort": sort,
            "page": 1
        };
    sendSearchReq(req_data);  
}

function nextPage(p) {
    var category = document.getElementById("category").value;
    var sort = document.getElementById("sort").value;
    var search_key = document.getElementById("keyword").value;
    var page;
    if(!isNaN(p)) {       //not a number
        page = parseInt(p);
    } else{
        current_page = parseInt(document.getElementById("current_page").value);
        last_page = parseInt(document.getElementById("last_page").value);
        if(p == "Next") {
            page = current_page + 1;
        } else if (p == "Previous") {
            page = current_page - 1;
        } else if (p == "First") {
            page = 1;
        } else{
            page = last_page;
        }
    }
    var req_data={
        "category": category,
        "search_key": search_key,
        "sort": sort,
        "page": page
    };
    sendSearchReq(req_data);
}

function sendSearchReq(req_data) {
    $.ajax({
        url: '/search',
        contentType: 'application/json;charset=UTF-8',
        type: 'GET',
        data: req_data,
        success: function(response) { 
            $('body').html(response);
        },
        error: function(error) {
            console.log(error);
        }
    }); 
}