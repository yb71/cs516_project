function backToShopping() {
    $.ajax({
        url: '/',
        contentType: 'application/json;charset=UTF-8',
        type: 'GET',
        success: function(response) {
            window.location.href = window.location.href.split("checkout")[0] + "/";
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function backToViewHistoryPurchase() {
    $.ajax({
        url: '/purchase_history?page=1',
        contentType: 'application/json;charset=UTF-8',
        type: 'GET',
        success: function(response) {
            window.location.href = window.location.href.split("checkout")[0] + "/purchase_history?page=1";
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function backToCart() {
    $.ajax({
        url: '/',
        contentType: 'application/json;charset=UTF-8',
        type: 'GET',
        success: function(response) {
            window.location.href = window.location.href.split("checkout")[0] + "cart";
        },
        error: function(error) {
            console.log(error);
        }
    });
}
