from flask import current_app as app
from datetime import datetime

class Item:
    def __init__(self, item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status):
        self.item_id = item_id
        self.stock_id = stock_id
        self.buyer_id = buyer_id
        self.order_num = order_num
        self.seller_id = seller_id
        self.quantity = quantity
        self.unit_price = unit_price
        self.order_time = order_time
        self.fulfill_time = fulfill_time
        self.status = status

    @staticmethod
    def check_exits_by_bid_stockid_status(bid, stock_id, status):
        rows = app.db.execute('''
            SELECT buyer_id, order_num, seller_id, quantity, unit_price, status
            FROM Item
            WHERE buyer_id = :bid
            AND stock_id = :stock_id
            AND status = :status
            ''', 
                bid = bid,
                stock_id = stock_id,
                status = status
        )
        return len(rows) > 0


    "used for insert item into cart"
    @staticmethod
    def insert_item(buyer_id, stock_id, seller_id, quantity, status):
        t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        app.db.execute(
            ''' 
            INSERT INTO Item ( buyer_id, stock_id, seller_id, quantity, status, create_time, update_time)  
            VALUES (:buyer_id, :stock_id, :seller_id, :quantity, :status, :create_time, :update_time )
            ''',
                buyer_id = buyer_id,
                stock_id = stock_id,
                seller_id = seller_id,
                quantity = quantity,
                status = status,
                create_time = t,
                update_time = t
        )

    "Used for check duplicated item in user's cart before insert new item"
    @staticmethod
    def select_by_bid_stockid_status(bid, stock_id, status):
        rows = app.db.execute('''
            SELECT item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status
            FROM Item
            WHERE buyer_id = :bid
            AND stock_id = :stock_id
            AND status = :status
            ''', 
                bid = bid,
                stock_id = stock_id,
                status = status
        )
        return Item(*(rows[0])) if rows is not None else None

    """ Used for updating existing item quantity when users add existing items into carts """
    @staticmethod
    def update_quantity_by_bid_stockid_status(bid, stock_id, status, new_quantity):
        t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        app.db.execute(
            ''' 
            UPDATE Item
            SET quantity = :quantity, update_time = :update_time
            WHERE buyer_id = :bid
                AND stock_id = :stock_id
                AND status = :status
            ''',
                quantity = new_quantity,
                update_time = t,
                bid = bid,
                stock_id = stock_id,
                status = status
        )


    """ Used for updating existing item quantity within user's cart"""
    @staticmethod
    def update_quantity_by_bid_itemid(bid, item_id, new_quantity):
        t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        app.db.execute(
            ''' 
            UPDATE Item
            SET quantity = :quantity, update_time = :update_time
            WHERE buyer_id = :bid
                AND item_id = :item_id
            ''',
                quantity = new_quantity,
                update_time = t,
                bid = bid,
                item_id = item_id
        )


    """ Used for deleting existing item within user's cart"""
    def delete_item_by_item_id(item_id):
        app.db.execute(
            ''' 
            DELETE FROM Item
            WHERE item_id = :item_id
            ''',
                item_id = item_id,
        )


    """Used for showing a given user's current items in cart"""
    @staticmethod
    def select_by_bid_status(bid, status): 
        rows = app.db.execute('''
            SELECT item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status
            FROM Item 
            WHERE buyer_id = :bid
            AND status = :status
            ''', 
            bid = bid,
            status = status
        )
        return [Item(*row) for row in rows]


    @staticmethod
    def select_by_item_id(item_id):
        rows = app.db.execute(
            ''' 
            SELECT item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status
            FROM Item
            WHERE item_id = :item_id
            ''',
                item_id = item_id,
        )
        return Item(*(rows[0])) if rows is not None else None


    """ Used for updating existing item status within user's cart after checkout"""
    @staticmethod
    def checkout_carts_items_by_itemid(item_id, new_order_num, final_unit_price, new_status, new_quantity):
        t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        app.db.execute(
            ''' 
            UPDATE Item
            SET order_time = :order_time, status = :new_status, update_time = :update_time, order_num = :new_order_num, unit_price = :final_unit_price, quantity = :new_quantity
            WHERE item_id = :item_id
            ''',
                order_time = t,
                new_status = new_status,
                update_time = t,
                new_order_num = new_order_num,
                final_unit_price = final_unit_price,
                item_id = item_id,
                new_quantity = new_quantity
        )

    @staticmethod
    def select_names_by_itemids(item_ids):
        sql = """SELECT item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status
                FROM Item
                WHERE item_id IN :item_ids
                """
        rows = app.db.execute(sql, item_ids = item_ids)
        return {x.item_id: x for x in [Item(*row) for row in rows]}
    
    @staticmethod
    def fulfill_by_itemid(item_id):
        t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            app.db.execute(
                ''' 
                UPDATE Item
                SET fulfill_time = :fulfill_time, status = 3, update_time = :update_time
                WHERE item_id = :item_id
                ''',
                    fulfill_time = t,
                    update_time = t,
                    item_id = item_id
            )
            return True
        except Exception as e:
            print(str(e))
            return False

    @staticmethod
    def select_by_order_num(order_num):
        rows = app.db.execute(
            ''' 
            SELECT item_id, stock_id, buyer_id, order_num, seller_id, quantity, unit_price, order_time, fulfill_time, status
            FROM Item
            WHERE order_num = :order_num
            ''',
            order_num=order_num
        )
        return rows

    @staticmethod
    def clear_cart_by_bid(buyer_id, status):
        try:
            app.db.execute(
                ''' 
                DELETE *
                FROM Item
                WHERE buyer_id = :buyer_id AND status = :status
                ''',
                buyer_id = buyer_id,
                status = status
            )
            return True
        except Exception as e:
            print(str(e))
            return False

