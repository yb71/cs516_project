from flask import current_app as app
from .stock import Stock
from ..constants import IN_CART, ORDERED, CART_ITEM_OUT_OF_STOCK, CART_ITEM_INVALID, CART_ITEM_VALID, ORDERED_ITEM_VALID

class ItemInfo:
    def __init__(self, item_id, stock_id, name, seller_id, cart_qty, update_time, stock_qty, unit_price, img, description, item_status):
        self.item_id = item_id
        self.stock_id = stock_id
        self.name = name
        self.seller_id = seller_id
        self.cart_qty = cart_qty
        self.update_time = update_time
        self.stock_qty = stock_qty
        self.unit_price = unit_price
        self.img = img
        self.description = description
        self.item_status = item_status

    """Used for showing a given user's checked items """
    @staticmethod
    def get_items_by_bid_order_num(buyer_id, order_num):
        status = ORDERED
        res_items = []
        items = app.db.execute(
            """ 
            WITH filtered_item as (
                SELECT item_id, stock_id, seller_id, quantity, update_time
                FROM Item 
                WHERE buyer_id = :bid
                AND status = :status
                AND order_num = :order_num
            )
            SELECT fi.item_id, fi.stock_id, s.name, fi.seller_id, fi.quantity as cart_qty, fi.update_time, s.quantity as stock_qty, s.price, s.image, s.description
            FROM filtered_item fi LEFT JOIN Stock s
            ON fi.stock_id = s.stock_id
            WHERE s.deleted != true
            """,
            bid = buyer_id,
            order_num = order_num,
            status = status
        )
        for item in items:
            res_items.append(ItemInfo(*item, ORDERED_ITEM_VALID))
        return res_items

    @staticmethod
    def get_items_in_cart_by_uid(buyer_id):
        status = IN_CART
        ready_to_checkout = True
        total_price = 0
        total_item_num = 0
        items_in_cart = []
        items = app.db.execute(
            """ 
            WITH filtered_item as (
                SELECT item_id, stock_id, seller_id, quantity, update_time
                FROM Item 
                WHERE buyer_id = :bid
                AND status = :status
            )
            SELECT fi.item_id, fi.stock_id, s.name, fi.seller_id, fi.quantity as cart_qty, fi.update_time, s.quantity as stock_qty, s.price, s.image, s.description
            FROM filtered_item fi LEFT JOIN Stock s
            ON fi.stock_id = s.stock_id
            WHERE s.deleted != true
            """,
            bid = buyer_id,
            status = status
        )
        for item in items:
            if item.stock_qty is None: 
                ready_to_checkout = False
                items_in_cart.append(ItemInfo(*item, CART_ITEM_INVALID))
            elif item[6] < item[4]:  
                ready_to_checkout = False
                items_in_cart.append(ItemInfo(*item, CART_ITEM_OUT_OF_STOCK))
                total_price += item[7] * item[4]
                total_item_num += item[4]
            else:
                items_in_cart.append(ItemInfo(*item, CART_ITEM_VALID))
                total_price += item[7] * item[4]
                total_item_num += item[4]
        if len(items_in_cart) == 0:
            ready_to_checkout = False
        return ready_to_checkout, items_in_cart, round(total_price,2), total_item_num

    @staticmethod
    def get_items_by_bid_itemids(buyer_id, item_ids):
        status = ORDERED
        res_items = []
        items = app.db.execute(
            """ 
            WITH filtered_item as (
                SELECT item_id, stock_id, seller_id, quantity, update_time
                FROM Item 
                WHERE buyer_id = :bid
                AND status = :status
                AND item_id IN :item_ids
            )
            SELECT fi.item_id, fi.stock_id, s.name, fi.seller_id, fi.quantity as cart_qty, fi.update_time, s.quantity as stock_qty, s.price, s.image, s.description
            FROM filtered_item fi LEFT JOIN Stock s
            ON fi.stock_id = s.stock_id
            WHERE s.deleted != 'true'
            """,
            bid = buyer_id,
            item_ids = item_ids,
            status = status
        )
        for item in items:
            res_items.append(ItemInfo(*item, ORDERED_ITEM_VALID))
        return res_items