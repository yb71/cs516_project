from flask import current_app as app
from datetime import datetime
from .item import Item

class Order:
    def __init__(self, order_num, buyer_id, seller_id, quantity, name, address, status, delivery_time, update_time):
        self.order_num = order_num
        self.buyer_id = buyer_id
        self.seller_id = seller_id
        self.quantity = quantity
        self.name = name
        self.address = address
        self.status = status
        self.delivery_time = delivery_time
        self.update_time = update_time
    
    @staticmethod
    def select_count_by_seller_id(seller_id):
        rows = app.db.execute(
            ''' 
            SELECT count(*)
            FROM Item as i JOIN 
            (SELECT user_id, address, name FROM Users) AS u
            ON i.buyer_id = u.user_id
            WHERE seller_id = :seller_id AND status != 1
            ''',
            seller_id = seller_id)
        return rows[0][0]

    @staticmethod
    def select_by_seller_id(seller_id):
        rows = app.db.execute(
            ''' 
            SELECT order_num, buyer_id, seller_id, name, address, status, update_time
            FROM Item as i JOIN 
            (SELECT user_id, address, name FROM Users) AS u
            ON i.buyer_id = u.user_id
            WHERE seller_id = :seller_id AND status != 1
            ''',
                seller_id = seller_id)
        if len(rows)<1:
            return None
        orders_d = {}
        for row in rows:
            if row[0] not in orders_d:
                orders_d[row[0]] = [row]
            else:
                l = orders_d[row[0]]
                l.append(row)
                orders_d[row[0]] = l
        orders = []
        for oid in orders_d.keys():
            l = orders_d[oid]
            status = 'Fulfilled'
            for row in l:
                if row[5] != 3:
                    #print(row[4])
                    status = 'Unfulfilled'
                    break
            delivery_time = 'Not Delivered'
            update_time = max([r[6].strftime("%Y-%m-%d %H:%M:%S") for r in l])
            if status == 'Fulfilled':
                delivery_time = 'Delivery Time: ' + update_time
            order_num = l[0][0]
            buyer_id = l[0][1]
            seller_id = l[0][2]
            quantity = len(l)
            name = l[0][3]
            address = l[0][4]
            orders.append(Order(order_num,buyer_id,seller_id,quantity,name,address,status,delivery_time,update_time))
        #return Item(*(rows[0])) if rows is not None else None
        return orders


    @staticmethod
    def select_by_seller_id_limited(seller_id, limit, offset):
        rows = app.db.execute(
            ''' 
            SELECT order_num, buyer_id, seller_id, name, address, status, update_time
            FROM Item as i JOIN 
            (SELECT user_id, address, name FROM Users) AS u
            ON i.buyer_id = u.user_id
            WHERE seller_id = :seller_id AND status != 1
            LIMIT :limit
            OFFSET :offset
            ''',
                seller_id = seller_id,
                limit = limit,
                offset = offset)
        if len(rows)<1:
            return None
        orders_d = {}
        for row in rows:
            if row[0] not in orders_d:
                orders_d[row[0]] = [row]
            else:
                l = orders_d[row[0]]
                l.append(row)
                orders_d[row[0]] = l
        orders = []
        for oid in orders_d.keys():
            l = orders_d[oid]
            status = 'Fulfilled'
            for row in l:
                if row[5] != 3:
                    #print(row[4])
                    status = 'Unfulfilled'
                    break
            delivery_time = 'Not Delivered'
            update_time = max([r[6].strftime("%Y-%m-%d %H:%M:%S") for r in l])
            if status == 'Fulfilled':
                delivery_time = 'Delivery Time: ' + update_time
            order_num = l[0][0]
            buyer_id = l[0][1]
            seller_id = l[0][2]
            quantity = len(l)
            name = l[0][3]
            address = l[0][4]
            orders.append(Order(order_num,buyer_id,seller_id,quantity,name,address,status,delivery_time,update_time))
        #return Item(*(rows[0])) if rows is not None else None
        return orders


    @staticmethod
    def get_count_by_seller_search(seller_id, input_text, from_date, to_date):
        input_text = '%'+input_text+'%'
        where_clause = "WHERE i.seller_id = :seller_id  AND status != 1"
        if input_text:
                where_clause += " AND (lower(s.name) LIKE :input_text OR lower(u.name) LIKE :input_text)"
        if from_date is not None:
                where_clause += " AND :from_date <= CAST(i.order_time as DATE)"
        if to_date is not None:
                where_clause += " AND :to_date >= CAST(i.order_time as DATE)"
        sql = f'''SELECT count(*)
                     FROM Item as i JOIN 
                     (SELECT user_id, address, name FROM Users) AS u
                     ON i.buyer_id = u.user_id
                     JOIN Stock as s ON i.stock_id = s.stock_id
                     {where_clause}
                  '''
        rows = app.db.execute(sql, seller_id=seller_id, input_text=input_text, from_date=from_date, to_date=to_date)
        return rows[0][0]
    

    @staticmethod
    def get_order_num_by_seller_search(seller_id, input_text, from_date, to_date, limit, offset):
        input_text = '%'+input_text+'%'
        where_clause = "WHERE i.seller_id = :seller_id  AND status != 1"
        if input_text:
                where_clause += " AND (lower(s.name) LIKE :input_text OR lower(u.name) LIKE :input_text)"
        if from_date is not None:
                where_clause += " AND :from_date <= CAST(i.order_time as DATE)"
        if to_date is not None:
                where_clause += " AND :to_date >= CAST(i.order_time as DATE)"
        sql = f'''SELECT order_num, buyer_id, i.seller_id, u.name, address, status, i.update_time as update_time
                     FROM Item as i JOIN 
                     (SELECT user_id, address, name FROM Users) AS u
                     ON i.buyer_id = u.user_id
                     JOIN Stock as s ON i.stock_id = s.stock_id
                     {where_clause}
                     LIMIT {limit}
                     OFFSET {offset}
                  '''
        rows = app.db.execute(sql, seller_id=seller_id, input_text=input_text, from_date=from_date, to_date=to_date)
        if len(rows)<1:
            return None
        orders_d = {}
        for row in rows:
            if row[0] not in orders_d:
                orders_d[row[0]] = [row]
            else:
                l = orders_d[row[0]]
                l.append(row)
                orders_d[row[0]] = l
        orders = []
        for oid in orders_d.keys():
            l = orders_d[oid]
            status = 'Fulfilled'
            for row in l:
                if row[5] != 3:
                    #print(row[4])
                    status = 'Unfulfilled'
                    break
            delivery_time = 'Not Delivered'
            update_time = max([r[6].strftime("%Y-%m-%d %H:%M:%S") for r in l])
            if status == 'Fulfilled':
                delivery_time = 'Delivery Time: ' + update_time
            order_num = l[0][0]
            buyer_id = l[0][1]
            seller_id = l[0][2]
            quantity = len(l)
            name = l[0][3]
            address = l[0][4]
            orders.append(Order(order_num,buyer_id,seller_id,quantity,name,address,status,delivery_time,update_time))
        #return Item(*(rows[0])) if rows is not None else None
        return orders



    @staticmethod
    def get_order_num_count_by_buyer_search(user_id, input_text, from_date, to_date):
        where_clause = "WHERE buyer_id = :user_id AND status != 1"
        if input_text is not None:
            where_clause += f" AND (LOWER(Stock.name) LIKE \'%{input_text}%\' OR LOWER(Stock.name) LIKE \'%{input_text}%\')"
        if from_date is not None:
            where_clause += f" AND :from_date <= CAST(order_time as DATE)"
        if to_date is not None:
            where_clause += f" AND :to_date >= CAST(order_time as DATE)"
        sql = f'''SELECT COUNT(DISTINCT order_num)
                 FROM Item JOIN Stock ON Item.stock_id = Stock.stock_id JOIN Users ON Item.seller_id = Users.user_id
                 {where_clause}
              '''
        rows = app.db.execute(sql, user_id=user_id, input_text=input_text, from_date=from_date, to_date=to_date)[0][0]
        return rows

    @staticmethod
    def get_order_num_by_buyer_search(user_id, input_text, from_date, to_date, limit, offset):
        where_clause = "WHERE buyer_id = :user_id AND status != 1"
        if input_text is not None:
            where_clause += f" AND (LOWER(Stock.name) LIKE \'%{input_text}%\' OR LOWER(Stock.name) LIKE \'%{input_text}%\')"
        if from_date is not None:
            where_clause += f" AND :from_date <= CAST(order_time as DATE)"
        if to_date is not None:
            where_clause += f" AND :to_date >= CAST(order_time as DATE)"
        sql = f'''SELECT order_num
                     FROM Item JOIN Stock ON Item.stock_id = Stock.stock_id JOIN Users ON Item.seller_id = Users.user_id
                     {where_clause}
                     GROUP BY order_num
                     LIMIT {limit}
                     OFFSET {offset}
                  '''
        rows = app.db.execute(sql, user_id=user_id, input_text=input_text, from_date=from_date, to_date=to_date)
        return rows
