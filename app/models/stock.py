from flask import current_app as app
from datetime import datetime


class Stock:
    def __init__(self, stock_id, name, seller_id, cid, quantity, image, price, description, create_time, update_time):
        self.stock_id = stock_id
        self.name = name
        self.seller_id = seller_id
        self.cid = cid
        self.quantity = quantity
        self.image = image
        self.price = price
        self.description = description
        self.create_time = create_time
        self.update_time = update_time

    @staticmethod
    def select_by_primary_key(stock_id):
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM (SELECT * FROM Stock WHERE deleted != 'true') AS s
            WHERE stock_id = :stock_id
            ORDER BY s.update_time DESC
            ''', stock_id=stock_id)
        return Stock(*(rows[0])) if rows is not None else None

    @staticmethod
    def update_quantity_by_stock_id(stock_id, new_quantity):
        app.db.execute('''
            UPDATE Stock 
            SET quantity = :new_quantity
            WHERE stock_id = :stock_id
        ''',
        new_quantity = new_quantity,
        stock_id = stock_id)


    @staticmethod
    def select_count_by_seller(seller_id):
        rows = app.db.execute('''
            SELECT count(*)
            FROM (SELECT * FROM Stock WHERE deleted != 'true') AS s
            WHERE s.seller_id = :seller_id
            ''', 
            seller_id = seller_id)
        if len(rows) < 1:
            return None
        return rows[0][0]

    @staticmethod
    def select_by_seller(seller_id):
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM (SELECT * FROM Stock WHERE deleted != 'true') AS s
            WHERE s.seller_id = :seller_id
            ORDER BY s.update_time DESC
            ''', 
            seller_id = seller_id)
        if len(rows) < 1:
            return None
        return [Stock(*row) for row in rows]
    
    @staticmethod
    def select_by_seller_limited(seller_id,limit,offset):
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM (SELECT * FROM Stock WHERE deleted != 'true') AS s
            WHERE s.seller_id = :seller_id
            ORDER BY s.update_time DESC
            LIMIT :limit
            OFFSET :offset
            ''', 
            seller_id = seller_id,
            limit = limit,
            offset = offset)
        if len(rows) < 1:
            return None
        return [Stock(*row) for row in rows]
        

    @staticmethod
    def get_all():
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM Stock s
            ORDER BY s.update_time DESC
            ''')
        return [Stock(*row) for row in rows]

    @staticmethod
    def get_stock_by_condition_with_pagination(pname_search, category, sort, limit, offset):
        where_clause = "WHERE deleted != \'true\'"
        sort_order = ""
        if pname_search:
             where_clause += f"AND LOWER(name) LIKE \'{pname_search}%\'"
        if category is not None and category >= 0:
            where_clause += f"AND cid = {category}" 
        if sort is not None and sort >=0: 
            if sort == 0:
                sort_order = "price,"
            if sort == 1:
                sort_order = "price DESC,"
        
        sql = f'''
            SELECT stock_id, name, seller_id, cid, quantity, image, price, description, create_time, update_time
            FROM Stock
            {where_clause}
            ORDER BY {sort_order} update_time DESC, name, quantity DESC
            LIMIT {limit}
            OFFSET {offset}
            '''
        rows = app.db.execute(sql)
        return [Stock(*row) for row in rows]

    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def get_total_count_condition(pname_search, category):
        where_clause = ""
        deleted = 'true'
        if pname_search:
            if not where_clause:
                where_clause += f"WHERE LOWER(name) LIKE \'{pname_search}%\'"
            else: 
                where_clause += f"AND LOWER(name) LIKE \'{pname_search}%\'"
        if category is not None and category >= 0:
            if not where_clause:
                where_clause += f"WHERE cid = {category}"
            else:
                where_clause += f"AND cid = {category}"
        
        sql = f'''
            SELECT count(s.stock_id)
            FROM (SELECT * FROM Stock WHERE deleted != 'true') AS s
            {where_clause}
            '''
        return app.db.execute(sql)[0][0]

    @staticmethod
    def get_stock_by_sid(sid):
        deleted = 'true'
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM Stock s
            WHERE stock_id = :sid AND deleted != :deleted
            ''',
        sid = sid,
        deleted = deleted)
        return Stock(*(rows[0]))


    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def get_stock_by_sellerid_cid(seller_id,cid):
        deleted = 'true'
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM Stock s
            WHERE seller_id = :seller_id AND cid = :cid AND deleted != :deleted
            ''',
        seller_id = seller_id,
        cid = cid,
        deleted = deleted)
        if len(rows) < 1:
            return None
        return Stock(*(rows[0]))

    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def update_quantity_by_sid_cid(sid, cid, new_quantity):
        #sid: stock_id
        app.db.execute('''
            UPDATE Stock 
            SET quantity = :new_quantity
            WHERE stock_id = :sid AND cid = :cid
        ''',
        new_quantity = new_quantity,
        sid = sid,
        cid = cid)
        
    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def update_description_by_sid_cid(sid, cid, new_description):
        #sid: stock_id
        app.db.execute('''
            UPDATE Stock 
            SET description = new_description
            WHERE stock_id = :sid AND cid = :cid
        ''',
        sid = sid,
        cid = cid)

    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def update_price_by_sid_cid(sid, cid, new_price):
        #sid: stock_id
        app.db.execute('''
            UPDATE Stock 
            SET price = new_price
            WHERE stock_id = :sid AND cid = :cid
        ''',
        sid = sid,
        cid = cid)

    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def update_inventory(sid,name,new_quantity,new_price,new_image,new_description,dt_string):
        try:
            sql = """UPDATE Stock
            SET quantity = :quantity, name = :name, price = :price, image = :image, description = :description, update_time = :dt_string
            WHERE stock_id = :sid
            """
            rows = app.db.execute(sql, quantity=new_quantity,
                                  name = name,
                                  image=new_image,
                                  description=new_description,
                                  price=new_price,
                                  dt_string=dt_string,
                                  sid = sid)
            return Stock.get_stock_by_sid(sid)

        except Exception as e:
            print(str(e))
            return None
    
    ############################# delete join product, p.name-->s.name, delete pid ######################
    @staticmethod
    def add_inventory(seller_id, name, cid, quantity, image, price, description, dt_string):
        try:
            deleted = "false"
            sql = """INSERT INTO Stock (seller_id,name,cid,quantity,image, price, description,create_time,update_time,deleted)
            VALUES (:seller_id,:name,:cid,:quantity,:image, :price, :description,:create_time,:update_time,:deleted)
            """
            rows = app.db.execute(sql, seller_id = seller_id,
                                  name = name,
                                  cid = cid,
                                  quantity=quantity,
                                  image=image,
                                  description=description,
                                  price=price,
                                  create_time=dt_string,
                                  update_time = dt_string,
                                  deleted = deleted)
            return True

        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def delete_inventory(stock_id):
        try:
            deleted = 'true'
            dt_string = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            sql = """UPDATE Stock
            SET deleted = :deleted, update_time = :dt_string
            WHERE stock_id = :stock_id
            """
            rows = app.db.execute(sql, stock_id = stock_id, deleted = deleted, dt_string = dt_string)
            return True     
        except Exception as e:
            print(str(e))
            return False     



    ############################# delete join product, p.name-->s.name, delete pid ######################
    """Deprecated--select_by_primary_key"""
    @staticmethod
    def select_by_seller_cid(seller_id, cid):
        deleted = 'true'
        rows = app.db.execute('''
            SELECT s.stock_id, s.name, s.seller_id, s.cid, s.cid, s.quantity, s.image, s.price, s.description, s.create_time, s.update_time
            FROM Stock s
            WHERE s.seller_id = :seller_id AND s.cid = :cid AND deleted != :deleted
            ORDER BY s.update_time DESC
            ''', 
            seller_id = seller_id,
            cid = cid,
            deleted = deleted)
        if len(rows) < 1:
            return None
        return Stock(*(rows[0]))







