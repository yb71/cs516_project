class Sort:
    def __init__(self, option, value, selected):
        self.option = option
        self.value = value
        self.selected = selected

    def sortOptions(selected_opt):
        OPTION_VALUE=[
            ["Sort by: Features", -1],
            ["Sort by: Price: Low to High", 0],
            ["Sort by: Price: High to Low", 1]]   
        res = []
        for option_value in OPTION_VALUE:
            option = option_value[0]
            value = option_value[1]
            selected = True if selected_opt == value else False
            res.append(Sort(option, value, selected))
        return res



    