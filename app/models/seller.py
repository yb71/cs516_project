from flask_login import UserMixin
from flask import current_app as app
from werkzeug.security import generate_password_hash, check_password_hash

from .. import login
class Seller:
    def __init__(self, seller_id, b_name, b_email, b_address):
        self.seller_id = seller_id
        self.b_name = b_name
        self.b_email = b_email
        self.b_address = b_address
    
    @staticmethod
    def get_all():
        rows = app.db.execute('''
            SELECT seller_id, b_name, b_email, b_address
            FROM Sellers
            ''')
        if len(rows) < 1:
            return None
        return [Seller(*row) for row in rows]

    @staticmethod
    def get_empty(seller_id):
        return Seller(seller_id,"","","")

    @staticmethod
    def get(seller_id):
        rows = app.db.execute('''
            SELECT seller_id, b_name, b_email, b_address
            FROM Sellers
            WHERE seller_id = :seller_id
            ''',
            seller_id = seller_id)
        if len(rows) < 1:
            return None
        return Seller(*(rows[0]))
    
    @staticmethod
    def add_seller(seller_id, b_name, b_email, b_address):
        try:
            sql = """INSERT INTO Sellers (seller_id, b_name, b_email, b_address)
            VALUES (:seller_id, :b_name, :b_email, :b_address)
            """
            rows = app.db.execute(sql, seller_id = seller_id,
                                  b_name = b_name,
                                  b_email = b_email,
                                  b_address = b_address)
            return True

        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def update_seller(seller_id, b_name, b_email, b_address):
        try:
            sql = """UPDATE Sellers
            SET b_name = :b_name, b_email = :b_email, b_address = :b_address
            WHERE seller_id = :seller_id
            """
            rows = app.db.execute(sql, seller_id = seller_id,
                                  b_name = b_name,
                                  b_email = b_email,
                                  b_address = b_address)
            return True

        except Exception as e:
            print(str(e))
            return None
    