from flask import current_app as app
from sqlalchemy.sql import text
from ..constants import IN_CART, ORDERED, DELIVERED_FULFILLED
from .stock import Stock
from .item import Item
import time
from datetime import datetime

class checkoutProcessor:
    def __init__():
        pass

    def checkout(buyer_id, request_dict, item_list, total_price):
        item_ids = tuple(set([item['item_id'] for item in item_list]))
        items_dict = Item.select_names_by_itemids(item_ids)
        uncheck_item_ids = []

        new_status = ORDERED
        total_price = 0
        new_order_num = str(buyer_id) + str(round(time.time() * 1000))
        for item in item_list:
            try:
                item_id = int(item['item_id'])
                unit_price = float(item['unit_price'])
                quantity = int(item['quantity'])
                sub_total_price = unit_price * quantity

                item_info = items_dict[item_id]
                # stock = Stock.select_by_primary_key(item_info.stock_id)
                # new_quantity = stock.quantity - quantity
                t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                
                with app.db.engine.begin() as conn:
                    conn.execute(text('''
                            UPDATE Stock 
                            SET quantity = quantity - :sub_quantity
                            WHERE stock_id = :stock_id
                        '''),
                        sub_quantity = quantity,
                        stock_id = item_info.stock_id)
                    
                    conn.execute(text(''' 
                        UPDATE Item
                        SET order_time = :order_time, status = :new_status, update_time = :update_time, order_num = :new_order_num, unit_price = :final_unit_price, quantity = :new_quantity
                        WHERE item_id = :item_id
                        '''),
                            order_time = t,
                            new_status = new_status,
                            update_time = t,
                            new_order_num = new_order_num,
                            final_unit_price = unit_price,
                            item_id = item_id,
                            new_quantity = quantity)
                    conn.execute(text( """UPDATE Users
                        SET balance = balance - :sub_total_price, update_time = :update_time
                        WHERE user_id = :user_id
                        """),
                            user_id=buyer_id,
                            sub_total_price=sub_total_price,
                            update_time=t)
                total_price = total_price + sub_total_price
            
            except Exception as e:
                print("Error in checking_processor with item_id = " + str(item_id) + ", Error msg = " + str(e))
                uncheck_item_ids.append(item_id)
        return new_order_num, uncheck_item_ids, total_price



        


