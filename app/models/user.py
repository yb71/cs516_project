from flask_login import UserMixin
from flask import current_app as app
from werkzeug.security import generate_password_hash, check_password_hash

from .. import login


class User(UserMixin):
    def __init__(self, user_id, email, name, address, balance):
        self.id = user_id
        self.email = email
        self.name = name
        self.address = address
        self.balance = balance

    @staticmethod
    def get_by_auth(email, password):
        rows = app.db.execute("""
SELECT password, user_id, email, name, address, balance
FROM Users
WHERE email = :email
""",
                              email=email)
        if not rows:  # email not found
            return None
        elif not check_password_hash(rows[0][0], password):
            # incorrect password
            return None
        else:
            return User(*(rows[0][1:]))

    @staticmethod
    def email_exists(email):
        rows = app.db.execute("""
SELECT email
FROM Users
WHERE email = :email
""",
                              email=email)
        return len(rows) > 0

    @staticmethod
    def register(email, password, name, address, dt_string):
        try:
            rows = app.db.execute("""
INSERT INTO Users(email, password, name, address, create_time, update_time)
VALUES(:email, :password, :name, :address, :dt_string, :dt_string)
RETURNING user_id
""",
                                  email=email,
                                  password=generate_password_hash(password),
                                  name=name,
                                  address=address,
                                  dt_string=dt_string)
            id = rows[0][0]
            return User.get(id)
        except Exception as e:
            # likely email already in use; better error checking and reporting needed;
            # the following simply prints the error to the console:
            print(str(e))
            return None

    @staticmethod
    @login.user_loader
    def get(user_id):
        rows = app.db.execute("""
SELECT user_id, email, name, address, balance
FROM Users
WHERE user_id = :user_id
""",
                              user_id=user_id)
        return User(*(rows[0])) if rows else None

    @staticmethod
    def select_names_by_ids(user_ids):
        sql = """SELECT user_id, name
        FROM Users
        WHERE user_id IN :user_ids
        """
        rows = app.db.execute(sql, user_ids = user_ids)
        return dict(rows)

    @staticmethod
    def update_info(user_id, email, name, address, dt_string):
        try:
            sql = """UPDATE Users
            SET email = :email, name = :name, address = :address, update_time = :dt_string
            WHERE user_id = :user_id
            """
            rows = app.db.execute(sql, user_id=user_id,
                                  email=email,
                                  name=name,
                                  address=address,
                                  dt_string=dt_string)
            return User.get(user_id)

        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def change_pw(user_id, pw, dt_string):
        try:
            sql = """UPDATE Users
            SET password = :password, update_time = :dt_string
            WHERE user_id = :user_id
            """
            rows = app.db.execute(sql, user_id=user_id,
                                  password=generate_password_hash(pw),
                                  dt_string=dt_string)
            return User.get(user_id)

        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def update_balance(user_id, fund, dt_string):
        try:
            sql = """UPDATE Users
            SET balance = :fund, update_time = :dt_string
            WHERE user_id = :user_id
            """
            rows = app.db.execute(sql, user_id=user_id,
                                  fund=fund,
                                  dt_string=dt_string)
            return User.get(user_id)

        except Exception as e:
            print(str(e))
            return None