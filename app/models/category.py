from flask import current_app as app

class Category:
    def __init__(self, category_id, name):
        self.category_id = category_id
        self.name = name

    @staticmethod
    def get(category_id):
        rows = app.db.execute('''
            SELECT category_id, name
            FROM Category
            WHERE category_id = :category_id
            ''', category_id=category_id)
        return Category(*(rows[0])) if rows is not None else None

    @staticmethod
    def get_all():
        rows = app.db.execute('''
            SELECT category_id, name
            FROM Category
            ''')
        return [Category(*row) for row in rows]
