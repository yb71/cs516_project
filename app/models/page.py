class Page:
    def __init__(self, page_num, current_selected, enabled):
        self.page_num = page_num
        self.current_selected = current_selected
        self.enabled = enabled

    def printing(self):
        print("page_num = " + str(self.page_num) + " current_selected = " + str(self.current_selected) + " enabled = " + str(self.enabled))

    def get_page_info(current_page, total_page_num):
        page_list = []
        #add previouse page
        if current_page == 1:
            page_list.append(Page("First", False, False))
            page_list.append(Page("Previous", False, False))
        else:
            page_list.append(Page("First", False, True))
            page_list.append(Page("Previous", False, True))

        min_page = max(1, current_page - 2)
        max_page = min(total_page_num, current_page + 2)
        for p in range(min_page, max_page + 1):
            if p == current_page:
                page_list.append(Page(str(p), True, True))
            else:
                page_list.append(Page(str(p), False, True))
        if current_page == total_page_num:
            page_list.append(Page("Next", False, False))
            page_list.append(Page("Last", False, False))
        else:
            page_list.append(Page("Next", False, True))
            page_list.append(Page("Last", False, True))
        return page_list
