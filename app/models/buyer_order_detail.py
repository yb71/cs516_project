from flask import current_app as app
from .stock import Stock
from ..constants import IN_CART, ORDERED, CART_ITEM_OUT_OF_STOCK, CART_ITEM_INVALID, CART_ITEM_VALID, ORDERED_ITEM_VALID


class BuyerOrderDetail:
    def __init__(self, item_id, stock_id, buyer_id, bname, pname, seller_id, sname, quantity, address, order_time, fulfill_time, price, img, status):
        self.item_id = item_id
        self.stock_id = stock_id
        self.buyer_id = buyer_id
        self.bname = bname
        self.pname = pname
        self.seller_id = seller_id
        self.sname = sname
        self.quantity = quantity
        self.address = address
        self.order_time = order_time
        self.fulfill_time = fulfill_time
        self.price = price
        self.img = img
        self.status = status

    

    @staticmethod
    def get_item_by_user_id_order_num(user_id, order_num):
        rows = app.db.execute(
            ''' 
            SELECT item_id, i.stock_id, i.buyer_id, bname, s.name, i.seller_id, sname, quantity, address, order_time, fulfill_time, unit_price, img, status
            FROM Item as i JOIN 
            (SELECT user_id as buyer_id, name AS bname, address FROM Users) AS b
            ON i.buyer_id = b.buyer_id
            JOIN
            (SELECT name, stock_id, image AS img FROM Stock WHERE deleted != 'true') AS s
            ON i.stock_id = s.stock_id
            JOIN
            (SELECT user_id as seller_id, name AS sname FROM Users) AS se
            ON i.seller_id = se.seller_id
            WHERE i.buyer_id = :user_id AND order_num = :order_num
            ''',
                user_id = user_id,
                order_num = order_num
        )
        if len(rows) < 1:
            return None
        return [BuyerOrderDetail(*row) for row in rows]