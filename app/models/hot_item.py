from flask import current_app as app
from datetime import datetime
from datetime import timedelta
class HotItem:

    def __init__(self, pname, unit_price, seller_id, stock_id, img, quantity):
        self.pname = pname
        self.unit_price = unit_price
        self.seller_id = seller_id
        self.stock_id = stock_id
        self.img = img
        self.quantity = quantity




    @staticmethod
    def select_week_hot_items(seller_id):
        n = datetime.now()
        d = timedelta(7)
        lw = n - d
        ut = n.strftime("%Y-%m-%d %H:%M:%S")
        lt = lw.strftime("%Y-%m-%d %H:%M:%S")
        rows = app.db.execute(
            ''' 
            SELECT name, unit_price, seller_id, stock_id, img, SUM(quantity) as quantity
            FROM
            (SELECT name, unit_price, seller_id, i.stock_id as stock_id, quantity, img
            FROM Item as i
            JOIN (SELECT name, image as img, stock_id FROM Stock WHERE deleted != 'true') as s
            ON s.stock_id = i.stock_id
            WHERE seller_id = :seller_id AND status = 3 AND fulfill_time <= :ut AND fulfill_time >= :lt) AS t
            GROUP BY name, unit_price, seller_id, stock_id, img
            ORDER BY quantity DESC, name ASC
            LIMIT 10
            ''',
                seller_id = seller_id,
                ut = ut,
                lt = lt
        )
        if len(rows)<1:
            return None
        return [HotItem(*row) for row in rows]